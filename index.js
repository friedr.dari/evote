const mysql = require('mysql2');
const { faker } = require('@faker-js/faker');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());
app.use(bodyParser.json());
const port = 3000;



const con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "12345678"
});

con.connect(async function(err) {
	if (err) throw err;
	console.log("Connected!");

	// Select or create database
	con.query("CREATE DATABASE IF NOT EXISTS wahl", function (err, result) {
		if (err) throw err;
		console.log("Database created");
	});

	con.query("USE wahl", function (err, result) {
		if (err) throw err;
		console.log("Using database wahl");
	});

	// Create tables

	// If the tables already exist, drop them first
	con.query("DROP TABLE IF EXISTS wahlkreis", function (err, result) {
		if (err) throw err;
		console.log("Table partei dropped");
	});

	con.query("DROP TABLE IF EXISTS partei", function (err, result) {
		if (err) throw err;
		console.log("Table partei dropped");
	});

	con.query("DROP TABLE IF EXISTS direktkandidat", function (err, result) {
		if (err) throw err;
		console.log("Table direktkandidat dropped");
	});

	con.query("DROP TABLE IF EXISTS wahlzettel", function (err, result) {
		if (err) throw err;
		console.log("Table wahlzettel dropped");
	});

	con.query("DROP TABLE IF EXISTS waehly", function (err, result) {
		if (err) throw err;
		console.log("Table waehly dropped");
	});

	// Create tables
	con.query(`
		CREATE TABLE partei (
			name TEXT NOT NULL,
			id INT AUTO_INCREMENT,
			wahlkreis INT NOT NULL,
			PRIMARY KEY (id)
		)
	`, function (err, result) {
		if (err) throw err;
		console.log("Table partei created");
	});

	con.query(`
		CREATE TABLE wahlkreis (
		    name TEXT NOT NULL,
		    id INT AUTO_INCREMENT,
			PRIMARY KEY (id)
		)
	`, function (err, result) {
		if (err) throw err;
		console.log("Table wahlkreis created");
	});

	con.query(`
		CREATE TABLE direktkandidat (
			name TEXT NOT NULL,
			id INT AUTO_INCREMENT,
			wahlkreis INT NOT NULL,
			PRIMARY KEY (id)
		)
	`, function (err, result) {
		if (err) throw err;
		console.log("Table direktkandidat created");
	});

	con.query(`
		CREATE TABLE wahlzettel (
		    id VARCHAR(255) NOT NULL,
		    wahlkreis INT NOT NULL,
			erststimme INT,
			zweitstimme INT,
            PRIMARY KEY (id)
		)
	`, function (err, result) {
		if (err) throw err;
		console.log("Table wahlzettel created");
 	});

	con.query(`
		CREATE TABLE waehly (
			name VARCHAR(255) NOT NULL,
			password TEXT NOT NULL,
			wahlkreis INT NOT NULL,
			hat_gewaehlt BIT DEFAULT 0,
			addresse TEXT NOT NULL,
            PRIMARY KEY (name)
		)
	`, function (err, result) {
		if (err) throw err;
		console.log("Table waehly created");
	});


	// Insert data
	const wahlkreis = [
		'Berlin',
		'Hamburg',
		'München',
		'Anderswo'
	];

	for (let i = 0; i < wahlkreis.length; i++) {
		await new Promise((resolve, reject) => {
			con.query("INSERT INTO wahlkreis (name) VALUES (?)", [wahlkreis[i]], function (err, result) {
				if (err) throw err;
				console.log("Inserted wahlkreis");
				wahlkreis[i] = result.insertId;
				resolve();
			});
		});
	}

	// Insert partei
	const partei = [
		'CDU',
		'SPD',
		'FDP',
		'Grüne',
		'Linke',
		'AfD'
	];
	for (let i = 0; i < partei.length; i++) {
		for (let j = 0; j < wahlkreis.length; j++) {
			if (Math.random() > 0.5) {
				con.query("INSERT INTO partei (name, wahlkreis) VALUES (?, ?)", [partei[i], wahlkreis[j]], function (err, result) {
					if (err) throw err;
					console.log("Inserted partei");
				});
			}
		}
	}

	// Insert direktkandidat
	const direktkandidat = [];
	for (let i = 0; i < 100; i++) {
		direktkandidat.push(faker.person.fullName());
	}

	for (let i = 0; i < direktkandidat.length; i++) {
		con.query("INSERT INTO direktkandidat (name, wahlkreis) VALUES (?, ?)", [direktkandidat[i], wahlkreis[Math.floor(Math.random() * wahlkreis.length)]], function (err, result) {
			if (err) throw err;
			console.log("Inserted direktkandidat");
		});
	}

	// Insert waehly
	const waehly = [];
	for (let i = 0; i < 100; i++) {
		waehly.push(faker.internet.userName());
	}

	for (let i = 0; i < waehly.length; i++) {
		const password = faker.internet.password();
		const address = faker.location.streetAddress(true);
		con.query("INSERT INTO waehly (name, password, wahlkreis, addresse) VALUES (?, ?, ?, ?)", [waehly[i], password, wahlkreis[Math.floor(Math.random() * wahlkreis.length)], address], function (err, result) {
			if (err) throw err;
			console.log(`Sende Anmeldedaten (${waehly[i]} ${password}) an ${address}`);
		});
	}

	// Insert test user
	con.query("INSERT INTO waehly (name, password, wahlkreis, addresse) VALUES (?, ?, ?, ?)", ['test', 'test', 1, 'hier'], function (err, result) {
		if (err) throw err;
		console.log("Inserted test user");
	});

	// App
	app.post('/login', (req, res) => {
		const { name, password } = req.body;
		con.query("SELECT * FROM waehly WHERE name = ? AND password = ?", [name, password], function (err, result) {
			if (err) throw err;
			if (result.length > 0) {
				res.send(JSON.stringify({ success: true, wahlkreis: result[0].wahlkreis }));
			} else {
				res.send(JSON.stringify({ success: false }));
			}
		});
	});

	app.get('/kandidaten', (req, res) => {
		const { wahlkreis } = req.query;
		con.query("SELECT * FROM direktkandidat WHERE wahlkreis = ?", [wahlkreis], function (err, result) {
			if (err) throw err;
			res.send(JSON.stringify(result));
		});
	});

	app.get('/parteien', (req, res) => {
		const { wahlkreis } = req.query;
		con.query("SELECT * FROM partei WHERE wahlkreis = ?", [wahlkreis], function (err, result) {
			if (err) throw err;
			res.send(JSON.stringify(result));
		});
	});

	app.post('/vote', (req, res) => {
		const { erststimme, zweitstimme, password, name } = req.body;
		// Check if the user is logged in
		con.query("SELECT * FROM waehly WHERE name = ? AND password = ?", [name, password], function (err, result) {
			if (err) throw err;
			if (result.length > 0) {
				const wahlkreis = result[0].wahlkreis;
				// Check if the user has already voted
				con.query("SELECT * FROM waehly WHERE name = ? AND hat_gewaehlt = 1", [name], function (err, result) {
					if (err) throw err;
					if (result.length > 0) {
						res.send(JSON.stringify({ success: false, message: 'You have already voted' }));
					} else {
						// Vote

						// Create a unique id for the vote
						const id = faker.string.uuid();
						con.query("INSERT INTO wahlzettel (id, wahlkreis, erststimme, zweitstimme) VALUES (?, ?, ?, ?)", [id, wahlkreis, erststimme, zweitstimme], function (err, result) {
							if (err) throw err;
							console.log("Inserted wahlzettel");
						});

						// Mark the user as voted
						con.query("UPDATE waehly SET hat_gewaehlt = 1 WHERE name = ?", [name], function (err, result) {
							if (err) throw err;
							console.log("Updated waehly");
						});

						res.send(JSON.stringify({ success: true, uuid: id }));
					}
				});
			} else {
				res.send(JSON.stringify({ success: false, message: 'You are not logged in' }));
			}
		});
	});

	app.get('/me/:uuid', (req, res) => {
		const { uuid } = req.params;
		con.query("SELECT kandidat.name as kandidat, partei.name as partei FROM wahlzettel LEFT JOIN direktkandidat kandidat ON wahlzettel.erststimme = kandidat.id LEFT JOIN partei ON wahlzettel.zweitstimme = partei.id WHERE wahlzettel.id = ?", [uuid], function (err, result) {
			if (err) throw err;
			if (result.length > 0) {
				res.send(JSON.stringify({ success: true, data: result[0] }));
			}
		});

		con.query("\n" +
			"SELECT\n" +
			"    wk.name AS wahlkreis_name,\n" +
			"    p.name AS partei_name,\n" +
			"    COUNT(w.zweitstimme) AS stimmen\n" +
			"FROM\n" +
			"    wahlzettel w\n" +
			"JOIN\n" +
			"    partei p ON w.zweitstimme = p.id\n" +
			"JOIN\n" +
			"    wahlkreis wk ON p.wahlkreis = wk.id\n" +
			"GROUP BY\n" +
			"    wk.name, p.name\n" +
			"ORDER BY\n" +
			"    wk.name, COUNT(w.zweitstimme) DESC;", [], function (err, result) {
			console.log(result)
		});
	});

	app.get('/results', (req, res) => {
		con.query("SELECT * FROM wahlzettel", function (err, result) {
			if (err) throw err;
			const data = result.map((item) => {
				return {
					erststimme: item.erststimme,
					zweitstimme: item.zweitstimme
				};
			});
			res.send(JSON.stringify({ success: true, data }));
		});
	});

	app.listen(port, () => {
		console.log(`Example app listening at http://localhost:${port}`);
	});
});
